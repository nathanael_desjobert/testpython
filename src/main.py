from typing import Optional
from fastapi import FastAPI
from mangum import Mangum

app = FastAPI()


@app.get("/{full_path}")
def capture_routes(full_path):
    return {"Hello": "World!!!"}

handler = Mangum(app)